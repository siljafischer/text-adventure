from django.contrib import admin
from django.urls import path, include
from . import views

app_name='textadventureapp'
urlpatterns = [
    path('', views.station, name='station'),
    path('<str:name>', views.station, name='station'),
]
